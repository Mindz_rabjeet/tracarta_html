﻿app = angular.module('app', ['uiSlider']);

app.controller('ItemCtrl', ['$scope', function ($scope) {
    $scope.item = {
        name: 'Potato',
        cost: 350,
        minAge: 25,
        maxAge: 450
    };
    $scope.currencyFormatting = function (value) { return value.toString() + " $"; };
}]);